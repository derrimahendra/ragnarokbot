import pyautogui as pg
import numpy as np
import cv2 as cv
from PIL import ImageGrab, Image
import time
from datetime import datetime
import mss
import mss.tools
import sys

def timing(f):
    def wrap(*args, **kwargs):
        time1 = time.time()
        ret = f(*args, **kwargs)
        time2 = time.time()
        print('{:s} function took {:.3f} ms'.format(
            f.__name__, (time2-time1)*1000.0))

        return ret
    return wrap

@timing
def capture():
    with mss.mss() as sct:
        # Get information of monitor 2
        monitor_number = 2
        mon = sct.monitors[monitor_number]

        # The screen part to capture
        monitor = {
            "top": mon["top"]+515,
            "left": 1480,
            "width": 200,
            "height": 200,
            "mon": monitor_number,
            "time": datetime.now().strftime("%S.%f"),
            "place": PLACE
        }
        output = "{place}-{time}.png".format(**monitor)

        # Grab the data
        sct_img = sct.grab(monitor)
        # mss.tools.to_png(sct_img.rgb, sct_img.size, output=output)
        img = np.array(sct_img)
        # cv.imshow("OpenCV", img)
        # cv.waitKey(0)
        # cv.imwrite(output, img)
        haystack_img = cv.cvtColor(img, cv.COLOR_RGB2BGR)
        # cv.imshow("OpenCV", haystack_img)
        # cv.waitKey(0)
        cv.imwrite("{place}-{time}.png".format(**monitor), haystack_img)

PLACE = str(sys.argv[1])

while 1:
    capture()
    time.sleep(0.1)
