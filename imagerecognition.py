import pyautogui as pg
import numpy as np
import cv2 as cv
from PIL import ImageGrab, Image
from datetime import datetime
import time
import mss
import mss.tools
import sys

def click(x,y):
    pg.moveTo(x,y)
    pg.click()

def timing(f):
    def wrap(*args, **kwargs):
        time1 = time.time()
        ret = f(*args, **kwargs)
        time2 = time.time()
        print('{:s} function took {:.3f} ms'.format(f.__name__, (time2-time1)*1000.0))

        return ret
    return wrap

# @timing
def check_state(needle_img):
    with mss.mss() as sct:
        # Get information of monitor 2
        monitor_number = 2
        mon = sct.monitors[monitor_number]

        # The screen part to capture
        monitor = {
            "top": mon["top"]+300,
            "left": 1300,
            "width": 500,
            "height": 500,
            "mon": monitor_number,
            "time": datetime.now().strftime("%S.%f"),
            "place": PLACE
        }
        output = "{place}-{time}.png".format(**monitor)
        sct_img = sct.grab(monitor)
        img = np.array(sct_img)
        haystack_img = cv.cvtColor(img, cv.COLOR_RGB2BGR)
        # cv.imshow("OpenCV", haystack_img)
        # cv.waitKey(0)

        result = cv.matchTemplate(haystack_img, needle_img, cv.TM_CCOEFF_NORMED)
        min_val, max_val, min_loc, max_loc = cv.minMaxLoc(result)

        if max_val >= THRESHOLD:
            needle_w = needle_img.shape[1]
            needle_h = needle_img.shape[0]

            top_left = max_loc
            bottom_right = (top_left[0] + needle_w, top_left[1] + needle_h)

            cv.rectangle(haystack_img, top_left, bottom_right, color=(0, 255, 0), thickness=2, lineType=cv.LINE_4)

            # You can view the processed screenshot like this:
            # cv.imshow('Result', haystack_img)
            # cv.waitKey()
            # Or you can save the results to a file.
            # cv.imwrite(output, haystack_img)

            return [1, max_val, str(max_loc), output]
        else:
            # print('Needle not found.')
            return [0, max_val, str(max_loc), output]

PLACE = str(sys.argv[1])
PICTURE_CV1 = cv.imread('./{}-cast.png'.format(PLACE))
PICTURE_CV2 = cv.imread('./{}-reel.png'.format(PLACE))
INTERVAL = 0.2
THRESHOLD = 0.75
last_state = 0

while 1:
    now = datetime.now()
    a, a_confidence, a_positions, a_output = check_state(PICTURE_CV1)
    b, b_confidence, b_positions, b_output = check_state(PICTURE_CV2)
    if b == 1:
        print("state: %i cast: %s reel: %s %s %s confidence: %s, position: %s, output: %s" % (last_state, a, b, "I can see reel", now.strftime("%m/%d/%Y, %H:%M:%S.%f"), b_confidence, b_positions, b_output))
        if last_state != 2:
            last_state = 2
            print("reel!!!!!!!!!!!!!!")
            click(1581,-472)
        time.sleep(INTERVAL)
    elif a == 1:
        print("state: %i cast: %s reel: %s %s %s confidence: %s, position: %s, output: %s" % (last_state, a, b, "I can see cast", now.strftime("%m/%d/%Y, %H:%M:%S.%f"), a_confidence, a_positions, a_output))
        if last_state != 1:
            last_state = 1
            print("cast!!!!!!!!!!!!!!")
            click(1581,-472)
        time.sleep(INTERVAL)
    else:
        print("state: %i cast: %s reel: %s %s %s confidence: %s, position: %s, output: %s" % (last_state, a, b, "I am unable to see it", now.strftime("%m/%d/%Y, %H:%M:%S.%f"), [a_confidence, b_confidence], [a_positions, b_positions], [a_output, b_output]))
        time.sleep(INTERVAL)
